import cadquery as cq
from cq_gears import SpurGear
from wg_classes import Screw
import wg_functions
import wg_shapes

# uchida corrugator
uchida_gear_outer_diameter = 22.0
uchida_gear_inner_diameter = 16.5
uchida_gear_teeth_height = (uchida_gear_outer_diameter - uchida_gear_inner_diameter) / 2
uchida_gear_ctc = 2 * uchida_gear_inner_diameter + 1

ball_bearing_outer_diameter = 32.0
ball_bearing_outer_diameter_including_margin = ball_bearing_outer_diameter + 0.075
ball_bearing_inner_diameter = 20.0
ball_bearing_depth = 7.0
ball_bearing_depth_including_margin = 7.0 + 0.3
ball_bearing_outer_ring_thickness = 1.3
ball_bearing_inner_ring_thickness = 1.7
ball_bearing_inner_depth = 6.0

# gear_steel_bore_Fchrod_diameter = None
gear_steel_bore_rod_diameter = 12.1 + 0.25  # margin

gear_module = None
gear_num_teeth = None

gear_ctc_margin = 0.3

# gear_teeth_depth = 3.0
# gear_teeth_depth = 2.5
# gear_teeth_depth = 2.0
gear_teeth_depth = 1.75
# gear_teeth_depth = 1.5

gear_steel_bore_rod_diameter_str = f"{round(gear_steel_bore_rod_diameter, 2)}-bore" if gear_steel_bore_rod_diameter else ""

gear_size_str = f"{gear_teeth_depth}-depth_{gear_ctc_margin}_ctc-margin{gear_steel_bore_rod_diameter_str}"

gears_ctc = 43.0
gear_target_diameter = gears_ctc + gear_teeth_depth - gear_ctc_margin
gears_center_z = gear_target_diameter + 6.0

print(f"gear_target_diameter={gear_target_diameter}")

edge_width = 100.0
edge_height = 2 * gears_center_z
edge_bot_extra_height = 25
edge_depth = 12
edge_screw_ctc = edge_width - 1.5 * edge_depth

edge_tension_block_width = 45
edge_tension_block_height_top = 35
edge_tension_block_height_bot = 18
edge_tension_block_margin = 0.3
edge_tension_block_height_margin = 3
edge_tension_screw_offset = 12

edge_tension_screw_diameter = 10
edge_tension_screw_axle_diameter = 5
edge_tension_screw_unloaded_length = 25
edge_tension_screw_s1_length = 7.5
edge_tension_screw_s2_length = 10
edge_tension_screw_s3_length = 11.25
edge_tension_screw_insert_diameter = 16
edge_tension_screw_insert_base_height = [
    4,
    4 + 0.5,
    4 + 1.0,
    4 + 1.5,
    4 + 2.0,
    4 + 2.5,
    # 4 + 5,
    # 4 + 7.5,
    # 4 + 10,
]

gear_depth = 85.0 + 2 * edge_depth
gear_top_depth = 10.0
gear_bot_depth = gear_depth - gear_top_depth

gear_to_side_margin = 1.5

base_width = edge_width * 2
base_height = gear_depth * 2
base_offset = edge_width / 3

handle_inner_tabs_width = ball_bearing_inner_diameter / 2
handle_shaft_tab_depth = 5
handle_shaft_length = gears_ctc + ball_bearing_inner_diameter
handle_length = 80
handle_depth = edge_depth + handle_shaft_tab_depth
gear_to_handle_base_depth = 25
screw_length = 30

printer_margin = 0.1


funnel_gap_z_max = 4
funnel_gap_z = 2

funnel_wall_thickness = 5 + funnel_gap_z_max - funnel_gap_z
funnel_length = 150
funnel_edge_fillet_inner_diameter = 4.1
# funnel_edge_fillet_diameter = 15
funnel_edge_fillet_diameter = funnel_edge_fillet_inner_diameter + 2 * funnel_wall_thickness

funnel_edge_support_length = funnel_length / 2

cavity_width = 40

funnel_screw_support_diameter = funnel_edge_fillet_diameter * 1.0
funnel_screw_y_offset = funnel_gap_z_max + funnel_screw_support_diameter / 2

funnel_edge_screw_x = [
    edge_width / 2,
    edge_width / 2 + funnel_edge_support_length - funnel_edge_fillet_diameter,
]

gear_alpha = 0.5

CHEAP = False
EXPORT = False

heatsert_diameter = Screw('m4').heatsert_diameter * 10
heatsert_long_depth = Screw('m4').heatsert_long_depth * 10
screw_head_diameter = Screw('m4').screw_head_diameter * 10
screw_head_thickness = Screw('m4').screw_head_thickness * 10
screw_diameter_tight = Screw('m4').screw_diameter_tight * 10
screw_diameter_loose = Screw('m4').screw_diameter_loose * 10

handle_heatsert_diameter = Screw('m6').heatsert_diameter * 10

###### Calculate modulu and num teeth through a combination of a loop and binary search

ka = 1.0  # Addendum coefficient
kd = 1.25 # Dedendum coefficient


best_z, best_m, best_error = None, None, None

if not CHEAP:
    for z in range(10, 100):
        m = 10.0
        step = 5.0
        for i in range(0, 24):
            # for m in [x / 1000.0 for x in range(500, 2000)]:
            # self.m = m = module
            # self.z = z = teeth_number
            clearance = 0.0

            d0 = m * z         # pitch diameter
            adn = ka / (z / d0) # addendum
            ddn = kd / (z / d0) # dedendum
            da = d0 + 2.0 * adn # addendum circle diameter
            dd = d0 - 2.0 * ddn - 2.0 * clearance # dedendum circle diameter

            ra = da / 2.0 # addendum radius
            rd = dd / 2.0 # dedendum radius

            outer_diameter_delta = gear_target_diameter - 2 * ra
            inner_diameter_delta = gear_target_diameter - 2 * gear_teeth_depth - 2 * rd

            pitch_diameter_delta = gear_target_diameter - gear_teeth_depth - 2 * ((ra - rd) / 2 + rd)

            error = outer_diameter_delta * outer_diameter_delta + inner_diameter_delta * inner_diameter_delta

            if best_error is None or error < best_error:
                best_error = error
                best_z = z
                best_m = m

            if pitch_diameter_delta > 0:
                m += step
            else:
                m -= step
            step = step / 2

    print(f"best_z={best_z}")
    print(f"best_m={best_m}")

    gear_module = best_m
    gear_num_teeth = best_z


def calc_ball_bearing():
    b = (
        cq.Workplane("XY")
            .circle(ball_bearing_outer_diameter / 2)
            .circle(ball_bearing_inner_diameter / 2)
            .extrude(ball_bearing_inner_depth)
            .translate((0, 0, (ball_bearing_depth - ball_bearing_inner_depth) / 2))
    )
    b = b.union((
        cq.Workplane("XY")
            .circle(ball_bearing_outer_diameter / 2)
            .circle(ball_bearing_outer_diameter / 2 - ball_bearing_outer_ring_thickness)
            .extrude(ball_bearing_depth)
    ))
    b = b.union((
        cq.Workplane("XY")
            .circle(ball_bearing_inner_diameter / 2 + ball_bearing_inner_ring_thickness)
            .circle(ball_bearing_inner_diameter / 2)
            .extrude(ball_bearing_depth)
    ))
    return b


def calc_teeth_depth_comparison():
    b = (
        cq.Workplane("XY")
            .circle(gear_target_diameter / 2)
            .circle((gear_target_diameter - 2 * gear_teeth_depth) / 2)
            .extrude(gear_depth)
            .translate((0, 0, -gear_depth))
    )
    return b


def calc_gear(depth):
    bore_d = gear_steel_bore_rod_diameter if gear_steel_bore_rod_diameter else 10
    if CHEAP:
        return (
            cq.Workplane("XY")
                .circle(gears_ctc / 2)
                .circle(bore_d / 2)
                .extrude(depth)
        )

    # Create a gear object with the SpurGear class
    spur_gear = (
        SpurGear(
            module=gear_module,
            pressure_angle=30,
            teeth_number=gear_num_teeth,
            width=depth,
            bore_d=bore_d,
            backlash=0.0,
            clearance=0.0,
        )
    )
    # print(f"spur_gear.r0={spur_gear.r0}")
    # print(f"spur_gear.ra={spur_gear.ra}")
    # print(f"spur_gear.rd={spur_gear.rd}")
    gear = cq.Workplane('XY').gear(spur_gear)

    if gear_steel_bore_rod_diameter is None:
        gear = gear.union((
            cq.Workplane("XY")
                .circle(bore_d / 2)
                .extrude(depth)
        ))

    return gear


# Build this gear using the gear function from cq.Workplane
gear_inner_bore_0 = (
    cq.Workplane("XY")
        .circle(ball_bearing_inner_diameter / 2)
)

if gear_steel_bore_rod_diameter:
    gear_inner_bore_0 = gear_inner_bore_0.circle(gear_steel_bore_rod_diameter / 2)

gear_inner_bore_1 = (
    cq.Workplane("XY")
        .circle(ball_bearing_inner_diameter / 2 + ball_bearing_inner_ring_thickness * 1.5)
)

if gear_steel_bore_rod_diameter:
    gear_inner_bore_1 = gear_inner_bore_1.circle(gear_steel_bore_rod_diameter / 2)

gear = (
    calc_gear(depth=-gear_depth + gear_to_side_margin * 2)
        .translate((0, 0, - gear_to_side_margin))
        .union((
        gear_inner_bore_0
            .extrude(gear_depth + (edge_depth) * 2)
            .translate((0, 0, - gear_depth - edge_depth))
    ))
        .union((
        gear_inner_bore_1
            .extrude(-gear_depth)
    ))
        .rotate((0, 0, 0), (1, 0, 0), 90)
)

for z in [-gear_to_side_margin, -gear_depth + gear_to_side_margin]:
    for r in range(0, 360, 60):
        x = wg_functions.middle_value(ball_bearing_inner_diameter, gear_steel_bore_rod_diameter) / 2
        gear = gear.cut((
            wg_shapes.filament_peg()
                .translate((x, 0, z))
                .rotate((0, 0, 0), (0, 0, 1), r)
                .rotate((0, 0, 0), (1, 0, 0), 90)
        ))

bb = (
    calc_ball_bearing()
        .rotate((0, 0, 0), (1, 0, 0), 90)
)

teeth_comparison = (
    calc_teeth_depth_comparison()
        .rotate((0, 0, 0), (1, 0, 0), 90)
)

teeth_comparison = (
    (
        teeth_comparison.translate((0, 0, edge_height / 2 + gears_ctc / 2))
    )
        .union((
        teeth_comparison.translate((0, 1, edge_height / 2 - gears_ctc / 2))
    ))
)

def calc_heatsert_with_screw_through(length=25):
    heatsert_with_screw_through = (
        (
            cq.Workplane("XY")
                .circle(screw_diameter_tight / 2)
                .extrude(length)
        )
            .union((
            cq.Workplane("XY")
                .circle(screw_diameter_tight / 2)
                .extrude(-edge_depth)
        ))
            .union((
            cq.Workplane("XY")
                .circle(screw_head_diameter / 2)
                .extrude(screw_head_thickness)
                .translate((0, 0, -edge_depth))
        ))
            .union((
            cq.Workplane("XY")
                .circle(screw_head_diameter / 2)
                .workplane(offset=(screw_head_diameter - screw_diameter_tight) / 2)
                .circle(screw_diameter_tight / 2)
                .loft(combine=True)
                .translate((0, 0, -edge_depth + screw_head_thickness))
        ))
            .union((
            cq.Workplane("XY")
                .circle(heatsert_diameter / 2)
                .extrude(heatsert_long_depth)
        ))
    )
    return heatsert_with_screw_through


heatsert_with_screw_through = calc_heatsert_with_screw_through()

edge_screws_base = (
    (
        heatsert_with_screw_through
            .translate((edge_screw_ctc / 2, -edge_depth / 2, 0))
    )
        .union((
        heatsert_with_screw_through
            .translate((-edge_screw_ctc / 2, -edge_depth / 2, 0))
    ))
        .translate((0, 0, -edge_height / 2 - edge_bot_extra_height / 2))
        .mirror(mirrorPlane='XY', union=True)
        .translate((0, 0, edge_height / 2 - edge_bot_extra_height / 2))
)

edge_funnel_screws = []
for x in funnel_edge_screw_x:
    for sign in [-1, 1]:
        edge_funnel_screws.append((
            calc_heatsert_with_screw_through(length=100)
                .rotate((0, 0, 0), (1, 0, 0), -90)
                .translate((x, 0, edge_height / 2 + sign * funnel_screw_y_offset))
        ))

edge_funnel_screws = wg_functions.union_shapes_list(edge_funnel_screws)

edge_screws = (
    edge_screws_base
        .union((
        edge_screws_base.translate((0, gear_depth + edge_depth))
    ))
        .union((
        (
            calc_heatsert_with_screw_through(length=20)
                .rotate((0, 0, 0), (0, 1, 0), 180)
                .translate((0, 0, gear_to_handle_base_depth + handle_shaft_tab_depth))
        )
            .rotate((0, 0, 0), (1, 0, 0), 90)
            .translate((0, 0, gears_ctc / 2 + edge_height / 2))
    ))
        .union(edge_funnel_screws)
    # .union((
    #     heatsert_with_screw_through
    #     .translate((0, 0, gear_to_handle_base_depth + handle_depth))
    #     .translate((0, -handle_shaft_length + ball_bearing_inner_diameter, 0))
    #     .rotate((0, 0, 0), (1, 0, 0), 90)
    #     .translate((0, 0, edge_height / 2 + gears_ctc / 2))
    # ))
)

edge = (
        cq.Workplane("XY")
            .rect(edge_width, edge_height)
            .pushPoints([(0, gears_ctc / 2), (0, -gears_ctc / 2)])
            .circle(ball_bearing_outer_diameter_including_margin / 2)
            .extrude(ball_bearing_depth_including_margin)
    )

if edge_bot_extra_height > 0:
    edge = (
        edge.union((
            cq.Workplane("XY")
            .rect(edge_width, edge_bot_extra_height)
            .extrude(edge_depth)
            .translate((0, -edge_width / 2 - edge_bot_extra_height / 2, 0))
        ))
    )

edge = (
    edge
        .union((
        cq.Workplane("XY")
            .rect(edge_width, edge_height)
            .pushPoints([(0, gears_ctc / 2), (0, -gears_ctc / 2)])
            .circle(ball_bearing_outer_diameter / 2 - ball_bearing_outer_ring_thickness * 1.5)
            .extrude(edge_depth - ball_bearing_depth_including_margin)
            .translate((0, 0, ball_bearing_depth_including_margin))
    ))
    .union((
        cq.Workplane("XY")
            .center(edge_width / 2, 0)
            .moveTo(0, edge_height / 3)
            .lineTo(funnel_edge_support_length, (funnel_screw_y_offset + funnel_screw_support_diameter / 2))
            .lineTo(funnel_edge_support_length, -(funnel_screw_y_offset + funnel_screw_support_diameter / 2))
            .lineTo(0, -edge_height / 3)
            .close()
            .extrude(edge_depth)
    ))
        .rotate((0, 0, 0), (1, 0, 0), 90)
        .translate((0, 0, gears_center_z))
        .cut(edge_screws)
)


def calc_edge_screw_tension_block(margin=0.0, top_extra=0.0, bot_extra=0.0):
    h_top = edge_tension_block_height_top + top_extra
    h_bot = edge_tension_block_height_bot + bot_extra
    return (
        cq.Workplane("XY")
        .rect(edge_tension_block_width + 2 * margin, h_top + h_bot + 2 * margin)
        .extrude(edge_depth)
        .translate(
            (0, - (h_top + h_bot) / 2 + h_bot,
             0))
        .rotate((0, 0, 0), (0, 0, 1), 180)
        .rotate((0, 0, 0), (1, 0, 0), 90)
        .translate((0, 0, 12))
        .edges("|Y")
        .chamfer(edge_depth * 1)
        .edges("|Z")
        .chamfer(edge_depth * 0.45)
    )

edge_screw_tension_block_intersect = calc_edge_screw_tension_block()
edge_screw_tension_block_cut = calc_edge_screw_tension_block(
    margin=edge_tension_block_margin,
    top_extra=edge_tension_block_height_margin,
    bot_extra=50,
)

edge_screw_tension_block = edge.intersect(edge_screw_tension_block_intersect)
edge = edge.cut(edge_screw_tension_block_cut)

edge_tension_screw = (
    cq.Workplane("XY")
    .circle(edge_tension_screw_diameter / 2 + edge_tension_block_margin)
    # .circle(edge_tension_screw_axle_diameter / 2 - edge_tension_block_margin)
    .extrude(edge_tension_screw_unloaded_length + edge_tension_block_margin)
    # .rotate((0, 0, 0), (1, 0, 0), 90)
    .translate((0, 0, -edge_bot_extra_height))
    .translate((0, 0, edge_tension_screw_insert_base_height[0]))
    .translate((0, -edge_depth/2, 0))
)


def calc_edge_tension_screw_tension_insert(base_height):
    return (
        cq.Workplane("XY")
        .circle(edge_tension_screw_insert_diameter / 2)
        .extrude(base_height)
        .union((
            cq.Workplane("XY")
            .circle(edge_tension_screw_axle_diameter / 2 - edge_tension_block_margin)
            .extrude(base_height + 5)
        ))
        .cut((
            cq.Workplane("XY")
                .text(txt=str(round(base_height, 2)), fontsize=6.0, distance=4, cut=True, clean=True)
                .rotate((0, 0, 0), (0, 1, 0), 180)
                .translate((0, 0, 1))
        ))
        # .rotate((0, 0, 0), (1, 0, 0), 90)
        .translate((0, 0, -edge_bot_extra_height))
        .translate((0, -edge_depth / 2, 0))
    )

edge_tension_screw_tension_inserts = []
for h in edge_tension_screw_insert_base_height:
    edge_tension_screw_tension_inserts.append(
        calc_edge_tension_screw_tension_insert(h)
    )

edge_screw_tension_block = edge_screw_tension_block.cut(edge_tension_screw)

edge_top = (
    (
        cq.Workplane("XY")
            .rect(edge_width, gear_depth + 2 * edge_depth)
            .extrude(edge_depth)
            .translate((0, gear_depth / 2, 0))
            .translate((0, 0, edge_height))
    )
        .cut(edge_screws)
)

hole_x = base_width / 2 - 15
hole_y = base_height / 2 - 15
edge_bot = (
    (
        cq.Workplane("XY")
            .rect(base_width, base_height)
            .pushPoints([
            (hole_x, hole_y),
            (hole_x, -hole_y),
            (-hole_x, hole_y),
            (-hole_x, -hole_y),
        ])
            .circle(screw_diameter_loose / 2)
            .extrude(-edge_depth)
            .translate((base_offset, 0, 0))
            .translate((0, gear_depth / 2, 0))
            .translate((0, 0, - edge_bot_extra_height))
    )
        .cut(edge_screws)
)

edge_left = edge
edge_right = (
    edge
        .mirror(mirrorPlane='XZ', union=False)
        .translate((0, gear_depth, 0))
)

gear_top = (
    gear
        .union((
        cq.Workplane("XY")
            .circle(ball_bearing_inner_diameter / 2)
            .extrude(gear_to_handle_base_depth + handle_shaft_tab_depth - edge_depth)
            .translate((0, 0, edge_depth))
            .rotate((0, 0, 0), (1, 0, 0), 90)
    ))
    .cut((
        cq.Workplane("XY")
            .circle(gear_steel_bore_rod_diameter / 2)
            .workplane(offset=(gear_steel_bore_rod_diameter - screw_diameter_tight) / 2)
            .circle(screw_diameter_tight / 2)
            .loft(combine=True)
            .translate((0, 0, edge_depth))
            .rotate((0, 0, 0), (1, 0, 0), 90)
    ))
        .cut((
        cq.Workplane("XY")
            .circle(ball_bearing_inner_diameter / 2)
            .extrude(handle_shaft_tab_depth)
            .cut((
            cq.Workplane("XY")
                .rect(handle_inner_tabs_width, ball_bearing_inner_diameter)
                .extrude(handle_shaft_tab_depth)
        ))
            .translate((0, 0, gear_to_handle_base_depth))
            .rotate((0, 0, 0), (1, 0, 0), 90)
    ))
        .translate((0, 0, gears_ctc / 2))
        .translate((0, 0, gears_center_z))
        .cut(edge_screws)
)
gear_bot = (
    gear
        .rotate((0, 0, 0), (0, 1, 0), 180 / (1 if CHEAP else gear_num_teeth))
        .translate((0, 0, -gears_ctc / 2))
        .translate((0, 0, gears_center_z))
)

gear_mid_solid = (
    cq.Workplane("XY")
        .circle(gear_target_diameter * 100)
        .extrude(-(gear_depth - 2 * gear_to_side_margin))
        .translate((0, 0, - gear_to_side_margin))
        .rotate((0, 0, 0), (1, 0, 0), 90)
)

gears_edge_bits = (
    gear_top.union(gear_bot)
        .cut(gear_mid_solid)
)

gear_top = gear_top.intersect(gear_mid_solid)
gear_bot = gear_bot.intersect(gear_mid_solid)

handle_shaft = (
    (
        cq.Workplane("XY")
            .rect(ball_bearing_inner_diameter, handle_shaft_length)
            .pushPoints([(0, -handle_shaft_length / 2 + ball_bearing_inner_diameter / 2)])
            .circle(handle_heatsert_diameter / 2)
            .extrude(handle_depth)
            .edges("|Z")
            .fillet(ball_bearing_inner_diameter * 0.45)
            # .cut((
            #     cq.Workplane("XY")
            #         .circle(screw_head_diameter / 2)
            #         .extrude(handle_depth)
            #         .translate((0, 0, -handle_depth / 2))
            #         .translate((0, -handle_shaft_length / 2 + ball_bearing_inner_diameter / 2, 0))
            # ))
            .cut((
            cq.Workplane("XY")
                .rect(handle_inner_tabs_width + 2 * printer_margin, ball_bearing_inner_diameter + 2 * printer_margin)
                .extrude(handle_shaft_tab_depth)
                .translate((0, +handle_shaft_length / 2 - ball_bearing_inner_diameter / 2, 0))
        ))
            .translate((0, -handle_shaft_length / 2 + ball_bearing_inner_diameter / 2, 0))
            .translate((0, 0, gear_to_handle_base_depth))
            .rotate((0, 0, 0), (1, 0, 0), 90)
            .translate((0, 0, edge_height / 2 + gears_ctc / 2))
            .cut(edge_screws)
    )
)

# handle = (
#     (
#         cq.Workplane("XY")
#             .circle(ball_bearing_inner_diameter * 0.75 / 2)
#             .extrude(handle_length)
#             .translate((0, 0, gear_to_handle_base_depth + handle_depth))
#             .translate((0, -handle_shaft_length + ball_bearing_inner_diameter, 0))
#             .rotate((0, 0, 0), (1, 0, 0), 90)
#             .translate((0, 0, edge_height / 2 + gears_ctc / 2))
#             .cut(edge_screws)
#     )
# )

funnel_edge_vert_fillet = (
    cq.Workplane("XY")
        .circle(funnel_edge_fillet_diameter / 2)
        .extrude(funnel_gap_z + funnel_edge_fillet_diameter)
        .cut((
            cq.Workplane("XY")
                .rect(funnel_edge_fillet_diameter, funnel_edge_fillet_diameter)
                .extrude(funnel_gap_z + funnel_edge_fillet_diameter)
                .translate((funnel_edge_fillet_diameter / 2, 0, 0))
        ))
        .translate((0, 0, -(funnel_gap_z + funnel_edge_fillet_diameter) / 2))
        .rotate((0, 0, 0), (0, 0, 1), -90)
        .rotate((0, 0, 0), (1, 0, 0), 90)
        .translate((funnel_length / 2, 0, 0))
        .translate((0, 0, edge_depth - funnel_edge_fillet_diameter / 2))
)

funnel_edge_solid = (
    (
        cq.Workplane("XY")
            .rect(funnel_length, funnel_gap_z + 2 * funnel_edge_fillet_diameter)
            .extrude(edge_depth)
    )
    .union((
        cq.Workplane("XY")
            .rect(funnel_length + funnel_edge_fillet_diameter, funnel_gap_z + 1 * funnel_edge_fillet_diameter)
            .extrude(edge_depth - funnel_edge_fillet_diameter / 2)
    ))
        .union((
        cq.Workplane("XY")
            .circle(funnel_edge_fillet_diameter * 0.5)
            .extrude(edge_depth)
            .translate((funnel_length / 2, 0, 0))
            .translate((0, funnel_gap_z / 2 + funnel_edge_fillet_diameter / 2, 0))
            .mirror(mirrorPlane='XZ', union=True)
    ))
)

funnel_screw_support = (
    cq.Workplane("XY")
        .circle(funnel_screw_support_diameter / 2)
        .extrude(cavity_width)
        .edges(">Z")
        .fillet(funnel_wall_thickness * 0.45)
)

funnel = (
    (
        cq.Workplane("XY")
            .rect(funnel_length, funnel_wall_thickness)
            .extrude(cavity_width)
            .edges(">Z")
            .fillet(funnel_wall_thickness * 0.45)
    )
        .union((
        cq.Workplane("XY")
            .circle(funnel_edge_fillet_diameter / 2)
            .extrude(cavity_width)
            .edges(">Z")
            .fillet(funnel_wall_thickness * 0.45)
            .translate((funnel_length / 2, - funnel_edge_fillet_diameter / 2 + funnel_wall_thickness / 2, 0))
    ))
        .translate((0, - funnel_gap_z / 2 - funnel_wall_thickness / 2, 0))

        .union((
            funnel_screw_support
                .translate((funnel_edge_screw_x[0] - funnel_length/2, - funnel_screw_y_offset, 0))
        ))
        .union((
            funnel_screw_support
                # .translate((funnel_edge_support_length / 2 - funnel_edge_fillet_diameter, - funnel_edge_fillet_diameter / 2 + funnel_wall_thickness / 2, 0))
                .translate((funnel_edge_screw_x[1] - funnel_length/2, - funnel_screw_y_offset, 0))
        ))

        .mirror(mirrorPlane='XZ', union=True)
        .union(funnel_edge_solid)
        .union(funnel_edge_vert_fillet)
        .translate((0, 0, -cavity_width / 2))
        .rotate((0, 0, 0), (1, 0, 0), 90)
        .translate((0, cavity_width / 2, 0))
        .translate((funnel_length / 2, 0, 0))
        .translate((0, 0, edge_height / 2))
)

for sign in [-1, 1]:
    funnel = funnel.cut((
        cq.Workplane("XY")
            .circle(gear_target_diameter / 2 + 2)
            .extrude(gear_depth)
            .rotate((0, 0, 0), (1, 0, 0), -90)
            .translate((0, 0, edge_height / 2 + sign * gears_ctc / 2))
    ))

funnel_left = (
    funnel
        .mirror(mirrorPlane='XZ', union=False)
        .translate((0, cavity_width, 0))
        .cut(edge_funnel_screws)
)

funnel_right = (
    funnel_left
        .mirror(mirrorPlane='XZ', union=False)
        .translate((0, gear_depth, 0))
)

bbs = (
    (
        bb.translate((0, 0, gears_ctc / 2))
    )
        .union((
        bb.translate((0, gear_depth + ball_bearing_depth, gears_ctc / 2))
    ))
        .union((
        bb.translate((0, 0, -gears_ctc / 2))
    ))
        .union((
        bb.translate((0, gear_depth + ball_bearing_depth, -gears_ctc / 2))
    ))
        .translate((0, 0, gears_center_z))
)

# show_object(gear, name=f'gear')
# show_object(edge, name=f'edge')
show_object(edge_left, name=f'edge_left')
show_object(edge_right, name=f'edge_right')
show_object(edge_top, name=f'edge_top')
show_object(edge_bot, name=f'edge_bot')

show_object(edge_screw_tension_block, name=f'edge_screw_tension_block', options={"color": (165, 40, 40), "alpha": 0.1})

for i in range(0, len(edge_tension_screw_tension_inserts)):
    show_object(edge_tension_screw_tension_inserts[i], name=f'edge_tension_screw_tension_insert_{i}', options={"color": (165, 40, 40), "alpha": 0.1})

show_object(edge_tension_screw, name=f'edge_tension_screw', options={"color": (40, 40, 165), "alpha": 0.5})

# show_object(funnel, name=f'funnel', options={"color": (165, 40, 40)})
show_object(funnel_left, name=f'funnel_left', options={"color": (165, 40, 40)})
show_object(funnel_right, name=f'funnel_right', options={"color": (165, 40, 40)})

show_object(handle_shaft, name=f'handle_shaft')
# show_object(handle, name=f'handle')

# show_object(edge_funnel_screws, name=f'edge_funnel_screws', options={"color": (40, 165, 40)})
show_object(edge_screws, name=f'edge_screws', options={"color": (40, 165, 40)})

# show_object(gear, name=f'gear')
show_object(gears_edge_bits, name=f'gears_edge_bits', options={"alpha": gear_alpha})
show_object(gear_top, name=f'gear_top', options={"alpha": gear_alpha})
show_object(gear_bot, name=f'gear_bot', options={"alpha": gear_alpha})
show_object(teeth_comparison, name=f'teeth_comparison', options={"color": (40, 40, 165), "alpha": 0.5})
# show_object(bb, name=f'bb', options={"color": (40, 40, 40)})
show_object(bbs, name=f'bbs', options={"color": (40, 40, 40)})


if EXPORT:
    cq.exporters.export(edge_left, f"edge_left.step")
    cq.exporters.export(edge_right, f"edge_right.step")
    cq.exporters.export(edge_top, f"edge_top.step")
    cq.exporters.export(edge_bot, f"edge_bot.step")
    cq.exporters.export(funnel_left, f"funnel_left.step")
    cq.exporters.export(funnel_right, f"funnel_right.step")
    cq.exporters.export(handle_shaft, f"handle_shaft.step")
    cq.exporters.export(gears_edge_bits, f"gears_edge_bits.step")
    cq.exporters.export(gear_top, f"gear_top_{gear_size_str}.step")
    cq.exporters.export(gear_bot, f"gear_bot_{gear_size_str}.step")

    cq.exporters.export(edge_screw_tension_block, f"edge_screw_tension_block.step")
    for i in range(0, len(edge_tension_screw_tension_inserts)):
        cq.exporters.export(edge_tension_screw_tension_inserts[i], f"edge_tension_screw_tension_inserts_{i}.step")
