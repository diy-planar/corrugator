import cadquery as cq

import wg_shapes


filament_peg_d_checker = (
    cq.Workplane("XY")
        .moveTo(-25, 0)
        .lineTo(-25, 5)
        .lineTo(25, 4)
        .lineTo(25, -4)
        .lineTo(-25, -5)
        .close()
        .extrude(6)
)

filament_peg_d_checker = (
    filament_peg_d_checker
        .cut(wg_shapes.filament_peg(d=2.3).translate((-20, 0, 0)))
        .cut(wg_shapes.filament_peg(d=2.2).translate((-10, 0, 0)))
        .cut(wg_shapes.filament_peg(d=2.1).translate((0, 0, 0)))
        .cut(wg_shapes.filament_peg(d=2.0).translate((10, 0, 0)))
        .cut(wg_shapes.filament_peg(d=1.9).translate((20, 0, 0)))
)


show_object(filament_peg_d_checker, name=f'filament_peg_d_checker')
cq.exporters.export(filament_peg_d_checker, f"filament_peg_d_checker.step")